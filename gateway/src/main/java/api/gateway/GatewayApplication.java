package api.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
@EnableEurekaClient
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

	@Bean
	@CrossOrigin("*")
	public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
		return builder.routes()
				.route(r -> r.path("/wish/**")
						.uri("http://localhost:9002/"))
				.route(r -> r.path("/wish_list/**")
						.uri("http://localhost:9002/"))
				.route(r -> r.path("/user/**")
						.uri("http://localhost:9003/"))
				.route(r -> r.path("/login")
						.uri("http://localhost:9003/"))
				.build();
	}
}
