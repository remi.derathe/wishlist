package api.gateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class AppControl {

    @GetMapping("/")
    public String test() {
        return "Hello JavaInUse Called in First Service";
    }
}
