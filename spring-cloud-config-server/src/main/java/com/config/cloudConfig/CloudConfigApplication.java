package com.config.cloudConfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@EnableConfigServer
public class CloudConfigApplication {

	public static void main(String[] arguments) {
		SpringApplication.run(CloudConfigApplication.class, arguments);
	}
}