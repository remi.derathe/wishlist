package api.user.dtos;

import api.user.model.Wish;
import api.user.model.WishList;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Collection;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
public class UserResponseDTO {
    Long id;
    String mail;
    String password;
    String pseudo;
    String presentation;
    Collection<Long> reservedId;
    Collection<Long> wishListId;
}
