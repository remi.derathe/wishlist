package api.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "`user`")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String mail;

    private String password;

    private String pseudo;

    private String presentation;

    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Role> appRoles=new ArrayList<>();

//    public Collection<GrantedAuthority>  getGrantedRole() {
//        Collection<GrantedAuthority> authorities = new ArrayList<>();
//        appRoles.forEach(role -> {
//            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
//        });
//
//        return authorities;
//    }

    @ElementCollection
    private Collection<Long> reservedId;

    @ElementCollection
    private Collection<Long> wishListId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;
        return id != null && Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
