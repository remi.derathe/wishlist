package api.user.mapper;

import api.user.dtos.UserResponseDTO;
import api.user.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserResponseDTO userToUserResponseDTO(User user);
}
