package api.user.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Collection;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
public class WishList {
    Long id;
    String name;
    String depiction;
    Date birthDate;
    String url;
    Collection<Wish> list;
    Long owner;
}
