package api.user.openFeign;

import api.user.model.Wish;
import api.user.model.WishList;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collection;

@FeignClient(name = "wishList-service")
public interface WishListClient {
    @GetMapping(value ="/wish_list/id={id}", consumes = "application/json")
    WishList getWishListById(@PathVariable Long id);

    @GetMapping(value ="/wish_list", consumes = "application/json")
    Collection<WishList> getAllUsers();

    @GetMapping(value ="/wish/id={id}", consumes = "application/json")
    Wish getWishById(@PathVariable Long id);
}
