package api.user.repository;

import api.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface UserRepo extends JpaRepository<User, Long> {
    @Query(value = "SELECT id FROM \"user\" u INNER JOIN user_reserved_id ur on :wishId = ur.reserved_id WHERE  ur.user_id = u.id", nativeQuery = true)
    Collection<Long> findByReservedId(@Param("wishId") Long wishId);

    User findByMail(String mail);
}
