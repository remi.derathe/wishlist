package api.user.service;

import api.user.dtos.UserResponseDTO;
import api.user.entity.Role;
import api.user.entity.User;

import java.util.Collection;

public interface UserService {
    User findById(Long id);

    User loadUserByUserMail(String userName);

    User save(User user);

    void delete(Long user);

    Role addNewRole(Role roleName);

    void addRoleToUser(String mail, String roleName);

    Collection<User> findAll();

    boolean addWishList(Long userId, Long wishListId);

    boolean removeWishList(Long userId, Long wishListId);

    boolean addReserved(Long userId, Long wishId);

    boolean removeReserved(Long userId, Long wishId);

    Collection<Long> removeByReservedWishId(Long wishId);

    String login(String mail, String password);

//    UserDetails loadUserByUsername(String username);
    Long getUserIdByToken(String token);
}
