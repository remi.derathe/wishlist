//package api.user.service.impl;
//
//import api.user.entity.Role;
//import api.user.repository.UserRepo;
//import api.user.service.SecurityService;
//import api.user.service.UserService;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.stereotype.Service;
//
//import javax.transaction.Transactional;
//import java.util.Collection;
//
//@Service
//@Transactional
//public class SecurityServiceImpl implements SecurityService {
//
//    @Override
//    public Authentication autoLogin(String username, String password, Collection<GrantedAuthority> roles) {
//
//        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(username, password, roles);
//
//        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
//            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
//        }
//        return usernamePasswordAuthenticationToken;
//    }
//
//
//}
