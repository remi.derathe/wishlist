package api.user.service.impl;

import api.user.dtos.UserResponseDTO;
import api.user.entity.Role;
import api.user.entity.User;
import api.user.exeption.ResourceNotFoundException;
import api.user.mapper.UserMapper;
import api.user.model.Wish;
import api.user.model.WishList;
import api.user.openFeign.WishListClient;
import api.user.repository.RoleRepository;
import api.user.repository.UserRepo;
import api.user.sec.JwtUtil;
import api.user.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;
    private final UserMapper userMapper;
    private final RoleRepository roleRepository;
    private final WishListClient wishListClient;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtil;
//    private final SecurityService securityService;

    @Override
    public User findById(Long id) {
        var user = userRepo.findById(id).orElseThrow(()->
                new ResourceNotFoundException("User not find!")
        );
        return user;
    }

    @Override
    public User loadUserByUserMail(String userName) {
        return userRepo.findByMail(userName);
    }

    @Override
    public Collection<User> findAll() {
        return userRepo.findAll();
    }

    @Override
    public User save(User user) {
        Role role = roleRepository.findByRoleName("USER");
        Collection<Role> roles = new ArrayList<>();
        roles.add(role);
        user.setAppRoles(roles);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepo.save(user);
    }

    @Override
    public void delete(Long user) {
        userRepo.deleteById(user);
    }

    @Override
    public Role addNewRole(Role roleName) {
        return roleRepository.save(roleName);
    }
    @Override
    public void addRoleToUser(String username, String roleName) {
        User appUser = userRepo.findByMail(username);
        Role appRole= roleRepository.findByRoleName(roleName);
        appUser.getAppRoles().add(appRole);
    }
    @Override
    public boolean addWishList(Long userId, Long wishListId) {
        Optional<User> user = userRepo.findById(userId);
        user.ifPresent(value -> value.getWishListId().add(wishListId));
        return user.isPresent();
    }

    @Override
    public boolean removeWishList(Long userId, Long wishListId) {
        Optional<User> user = userRepo.findById(userId);
        user.ifPresent(value -> value.getWishListId().remove(wishListId));
        return user.isPresent();
    }

    @Override
    public boolean addReserved(Long userId, Long wishId) {
        Optional<User> user = userRepo.findById(userId);
        user.ifPresent(value -> value.getReservedId().add(wishId));
        return user.isPresent();
    }

    @Override
    public boolean removeReserved(Long userId, Long wishId) {
        Optional<User> user = userRepo.findById(userId);
        user.ifPresent(value -> value.getReservedId().remove(wishId));
        return user.isPresent();
    }

    @Override
    public Collection<Long> removeByReservedWishId(Long wishId) {
        var users = userRepo.findByReservedId(wishId);
        for (Long id : users) {
            var wish = userRepo.findById(id);
            wish.ifPresent(user -> user.getReservedId().remove(wishId));
        }
        return users;
    }

    @Override
    public String login(String mail, String password) {
        User user = userRepo.findByMail(mail);
        String token = null;
        if (passwordEncoder.matches(password, user.getPassword())) {
//            Collection<GrantedAuthority> authorities = new ArrayList<>();
//            user.getAppRoles().forEach(role -> {
//                authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
//            });user.getMail()
            token = jwtUtil.generateJwtToken(
                    user.getMail()
                    //securityService.autoLogin(user.getMail(), password, authorities)
            );

        }
        return token;
    }

    @Override
    public Long getUserIdByToken(String token) {
        System.out.println(token);
        String mail = jwtUtil.getUserNameFromJwtToken(token);
        User user = userRepo.findByMail(mail);
        if (user != null) return user.getId();
        return null;
    }
}
