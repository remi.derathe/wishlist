package api.user.web;

import api.user.dtos.UserResponseDTO;
import api.user.entity.User;
import api.user.model.WishList;
import api.user.service.UserService;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
@RestController
@RequestMapping(value = "", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = "application/json")
public class UserControl {
    private final UserService userService;

    public UserControl(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UserLoginForm userLoginForm) {

        return new ResponseEntity<>(userService.login(userLoginForm.userMail, userLoginForm.password), HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/user/id={id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<User> getById(@PathVariable Long id) {
        System.out.println(id);
        return new ResponseEntity<>(userService.findById(id), HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/user", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Collection<User>> getAll() {
        return new ResponseEntity<>(userService.findAll(), HttpStatus.ACCEPTED);
    }

    @PostMapping("/user")
    public ResponseEntity<User> create(@RequestBody User user) {
        return new ResponseEntity<>(userService.save(user), HttpStatus.CREATED);
    }

    @PutMapping("/user")
    public ResponseEntity<User> update(@RequestBody User user) {
        return new ResponseEntity<>(userService.save(user), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/user")
    public ResponseEntity<Boolean> delete(@RequestParam("id") Long userId) {
        userService.delete(userId);
        return new ResponseEntity<>(true, HttpStatus.ACCEPTED);
    }

    @PutMapping("/user/addWishList")
    public ResponseEntity<Boolean> addWishList(
            @RequestParam Long userId,
            @RequestParam Long wishListId
    ) {
        return new ResponseEntity<>(userService.addWishList(userId, wishListId), HttpStatus.ACCEPTED);
    }

    @PutMapping("/user/removeWishList")
    public ResponseEntity<Boolean> removeWishList(
            @RequestParam Long userId,
            @RequestParam Long wishListId
    ) {
        return new ResponseEntity<>(userService.removeWishList(userId, wishListId), HttpStatus.ACCEPTED);
    }


    @PutMapping("/user/addReserved")
    public ResponseEntity<Boolean> addReserved(
            @RequestParam Long userId,
            @RequestParam Long wishId
    ) {
        return new ResponseEntity<>(userService.addReserved(userId, wishId), HttpStatus.ACCEPTED);
    }

    @PutMapping("/user/removeReserved")
    public ResponseEntity<Boolean> removeReserved(
            @RequestParam Long userId,
            @RequestParam Long wishId
    ) {
        return new ResponseEntity<>(userService.removeReserved(userId, wishId), HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/user/removeByReservedWishId")
    public ResponseEntity<Boolean> removeByReservedWishId(
            @RequestParam Long wishId
    ) {
        Collection<Long> users = userService.removeByReservedWishId(wishId);
        boolean userBool = users != null;
        return new ResponseEntity<>(userBool, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/user/getUserIdByToken", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Long> getUserIdByToken(
            @RequestParam String token
    ) {
        return new ResponseEntity<>(userService.getUserIdByToken(token), HttpStatus.ACCEPTED);
    }

}
@Data
class UserLoginForm{
    String userMail;
    String password;
}
