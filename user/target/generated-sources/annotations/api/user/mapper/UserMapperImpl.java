package api.user.mapper;

import api.user.dtos.UserResponseDTO;
import api.user.entity.User;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-11-30T11:58:10+0100",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.5 (Amazon.com Inc.)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public UserResponseDTO userToUserResponseDTO(User user) {
        if ( user == null ) {
            return null;
        }

        UserResponseDTO userResponseDTO = new UserResponseDTO();

        userResponseDTO.setId( user.getId() );
        userResponseDTO.setMail( user.getMail() );
        userResponseDTO.setPassword( user.getPassword() );
        userResponseDTO.setPseudo( user.getPseudo() );
        userResponseDTO.setPresentation( user.getPresentation() );

        return userResponseDTO;
    }
}
