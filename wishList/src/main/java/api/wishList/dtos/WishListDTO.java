package api.wishList.dtos;

import api.wishList.entity.Wish;
import api.wishList.model.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Collection;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
public class WishListDTO {
    Long id;
    String name;
    String depiction;
    long birthDate;
    String url;
    Collection<Wish> list;
    User owner;
}
