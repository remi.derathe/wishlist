package api.wishList.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
public class WishList {
    public WishList() {
        this.birthDate = new Date().getTime();
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String depiction;

    public String getDepiction() {
        return depiction;
    }

    public void setDepiction(String depiction) {
        this.depiction = depiction;
    }

    private long birthDate;

    public long getBirthDate() {
        return birthDate;
    }

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @OneToMany
    private Collection<Wish> list;

    public Collection<Wish> getList() {
        return list;
    }

    public Collection<Wish> setList(Collection<Wish> list) {
        this.list = list;
        return this.list;
    }

    private Long ownerId;

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long owner) {
        this.ownerId = owner;
    }
}
