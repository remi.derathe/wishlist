package api.wishList.mapper;

import api.wishList.dtos.WishListDTO;
import api.wishList.entity.WishList;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface WishListMapper {
    WishListDTO wishListToResponseDTO(WishList wishList);
}
