package api.wishList.model;

import api.wishList.entity.Wish;
import api.wishList.entity.WishList;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Collection;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
public class User {
    Long id;
    String mail;
    String pseudo;
    String presentation;
    Collection<Wish> reserved;
    Collection<WishList> wishListId;
}
