package api.wishList.openFeign;

import api.wishList.model.User;
import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@FeignClient(name = "user-service")
public interface UserClient {
    @GetMapping("/user/id={id}")
    User getUserById(@PathVariable Long id);

    @PutMapping(value = "/user/addWishList", consumes = "application/json")
    boolean addWishList(@RequestParam Long userId, @RequestParam Long wishListId);

    @PutMapping(value = "/user/removeWishList", consumes = "application/json")
    boolean removeWishList(@RequestParam Long userId, @RequestParam Long wishListId);

    @PutMapping(value = "/user/addReserved", consumes = "application/json")
    boolean addReserved(@RequestParam Long userId, @RequestParam Long wishId);

    @PutMapping(value = "/user/removeReserved", consumes = "application/json")
    boolean removeReserved(@RequestParam Long userId, @RequestParam Long wishId);

    @GetMapping(value = "/user/removeByReservedWishId", consumes = "application/json")
    void removeByReservedWishId(@RequestParam Long wishId);

//    @GetMapping("/user/loadUserByUsername")
//    UserDetails loadUserByUsername(String name);

}
