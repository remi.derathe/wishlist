package api.wishList.repository;

import api.wishList.entity.Wish;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WishRepo extends JpaRepository<Wish, Long>{
}
