//package api.wishList.sec.filters;
//
//import api.wishList.openFeign.UserClient;
//import io.micrometer.core.lang.NonNullApi;
//import lombok.AllArgsConstructor;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//@Component
//@AllArgsConstructor
//@NonNullApi
//public class JwtAuthorizationFilter extends OncePerRequestFilter {
//    private final JwtUtil jwtUtil;
//    private final UserClient userClient;
//
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//        String token = request.getHeader(JwtUtil.AUTH_HEADER);
//        if (token != null && token.startsWith(JwtUtil.PREFIX)) {
//            try {
//                String jwt = parseJwt(request);
//                if (jwtUtil.validateJwtToken(jwt)) {
//                    String name = jwtUtil.getUserNameFromJwtToken(jwt);
//
//                    UserDetails userDetails = userClient.loadUserByUsername(name);
//
//                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
//                            userDetails, userDetails.getPassword(), userDetails.getAuthorities());
//                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//
//                    SecurityContextHolder.getContext().setAuthentication(authentication);
//                }
//            } catch (Exception e) {
//                System.out.println(e);
//            }
//        }
//        filterChain.doFilter(request, response);
//    }
//    private String parseJwt(HttpServletRequest request) {
//        String headerAuth = request.getHeader("Authorization");
//
//        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
//            return headerAuth.substring(7);
//        }
//
//        return "null";
//    }
//}
