//package api.wishList.sec.filters;
//
//import io.jsonwebtoken.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.core.Authentication;
//import org.springframework.stereotype.Component;
//
//import java.util.Date;
//
//@Component
//public class JwtUtil {
//    @Value("${jwt.SECRET_KEY}")
//    public static final String SECRET_KEY = "9@AK$kT9KEtdqTT$8iT35I%jaYH4d3%Xl19cCSVQ3QjbqsV8x#R3wt$6XnCFk76Ib7mfdQ1h8Mq$Mr3zbbZNryZ1M4a1n@fBB#YA";
//    @Value("${jwt.AUTH_HEADER}")
//    public static final String AUTH_HEADER = "Authorization";
//    @Value("${jwt.PREFIX}")
//    public static final String PREFIX = "Bearer ";
//    private static final Logger logger = LoggerFactory.getLogger(JwtUtil.class);
//
//    public String generateJwtToken(Authentication authentication) {
//        int jwtExpirationMs = 360000;
//        return Jwts.builder()
//                .setSubject(authentication.getPrincipal().toString())
//                .setIssuedAt(new Date(System.currentTimeMillis()))
//                .setExpiration(new Date(System.currentTimeMillis() + jwtExpirationMs))
//                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
//                .compact();
//    }
//
//    public boolean validateJwtToken(String authToken) {
//        try {
//            Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(authToken);
//            return true;
//        } catch (SignatureException e) {
//            logger.error("Invalid JWT signature: {}", e.getMessage());
//        } catch (MalformedJwtException e) {
//            logger.error("Invalid JWT token: {}", e.getMessage());
//        } catch (ExpiredJwtException e) {
//            logger.error("JWT token is expired: {}", e.getMessage());
//        } catch (UnsupportedJwtException e) {
//            logger.error("JWT token is unsupported: {}", e.getMessage());
//        } catch (IllegalArgumentException e) {
//            logger.error("JWT claims string is empty: {}", e.getMessage());
//        }
//
//        return false;
//    }
//    public String getUserNameFromJwtToken(String token) {
//        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody().getSubject();
//    }
//}
