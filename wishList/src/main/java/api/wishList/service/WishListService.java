package api.wishList.service;

import api.wishList.dtos.WishListDTO;
import api.wishList.entity.Wish;
import api.wishList.entity.WishList;
import api.wishList.model.RequestAddWish;

import java.util.Collection;

public interface WishListService {
    WishList findById(Long id);

    WishListDTO findByIdDTO(Long id);

    WishList save(WishList wishList);

    void delete(Long wishListId);

    Collection<WishList> findAll();

    WishList addWish(Long idWishList,Long idWish);

    WishList removeWish(Long idWishList,Long idWish);
}
