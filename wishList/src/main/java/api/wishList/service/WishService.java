package api.wishList.service;

import api.wishList.entity.Wish;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public interface WishService {

    Wish findById(Long id);

    Wish save(Wish wish);

    void deleteById(Long id);

    Collection<Wish> findAll();
}
