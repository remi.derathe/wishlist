package api.wishList.service.impl;

import api.wishList.dtos.WishListDTO;
import api.wishList.entity.Wish;
import api.wishList.entity.WishList;
import api.wishList.mapper.WishListMapper;
import api.wishList.model.RequestAddWish;
import api.wishList.openFeign.UserClient;
import api.wishList.repository.WishListRepo;
import api.wishList.service.WishListService;
import api.wishList.service.WishService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class WishListServiceImpl implements WishListService {
    private final WishListRepo wishListRepo;
    private final WishService wishService;
    private final WishListMapper wishListMapper;
    private final UserClient userClient;

    //TODO: Debug
//    @Value("${api.host.baseurl:#{null}}")
//    private String baseUrl;

    @Override
    public WishList findById(Long id) {
        Optional<WishList> wishList = wishListRepo.findById(id);
        return wishList.orElse(null);
    }

    @Override
    public WishListDTO findByIdDTO(Long id) {
        Optional<WishList> wishList = wishListRepo.findById(id);
        if (wishList.isPresent()) {
            WishListDTO wishListDTO = wishListMapper.wishListToResponseDTO(wishList.get());
            wishListDTO.setOwner(userClient.getUserById(wishList.get().getOwnerId()));
            return wishListDTO;
        }
        return null;
    }

    @Override
    public WishList save(WishList wishs) {
        WishList list = wishListRepo.save(wishs);
        list.setUrl("http://localhost:8080/wish_list/list/"+list.getName()+"-"+list.getId());
        System.out.println("---------------"+list.getOwnerId()+ " "+list.getId());
        userClient.addWishList(list.getOwnerId(), list.getId());
        return list;
    }

    @Override
    public void delete(Long wishListId) {
        Optional<WishList> wishList = wishListRepo.findById(wishListId);
        if (wishList.isPresent()) {
            Long ownerId = wishList.get().getOwnerId();
            Collection<Wish> list = wishList.get().getList();
            list.forEach((wish) -> wishService.deleteById(wish.getId()));
            userClient.removeWishList(ownerId, wishListId);
            wishListRepo.delete(wishList.get());
        }
    }

    @Override
    public Collection<WishList> findAll() {
        return wishListRepo.findAll();
    }

    @Override
    public WishList addWish(Long idWishList, Long idWish) {
        Optional<WishList> wishList = wishListRepo.findById(idWishList);
        if (wishList.isPresent()) {
            Wish wish = wishService.findById(idWish);
            Collection<Wish> collection = wishList.get().getList();
            collection.add(wish);
            wishList.get().setList(collection);
            return wishList.get();
        }
        return null;
    }

    @Override
    public WishList removeWish(Long idWishList,Long idWish) {
        Optional<WishList> collection = wishListRepo.findById(idWishList);

        if (collection.isPresent()) {
            Wish wish = wishService.findById(idWish);
            Collection<Wish> filteredCollection = collection.get().getList()
                    .stream()
                    .filter(e -> !e.equals(wish)).toList();

            collection.get().setList(filteredCollection);
            wishService.deleteById(wish.getId());
            return
                    collection.get();
        }
        return null;
    }
}
