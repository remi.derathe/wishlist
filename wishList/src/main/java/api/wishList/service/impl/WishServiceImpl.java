package api.wishList.service.impl;

import api.wishList.entity.Wish;
import api.wishList.model.User;
import api.wishList.openFeign.UserClient;
import api.wishList.repository.WishRepo;
import api.wishList.service.WishService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class WishServiceImpl implements WishService {
    private final WishRepo wishRepo;
    private final UserClient userClient;

    public WishServiceImpl(WishRepo wishRepo, UserClient userClient) {
        this.wishRepo = wishRepo;
        this.userClient = userClient;
    }

    @Override
    public Wish findById(Long id) {
        return wishRepo.findById(id).orElse(null);
    }

    @Override
    public Collection<Wish> findAll() {
        return wishRepo.findAll();
    }

    @Override
    public Wish save(Wish wish) {
        return wishRepo.save(wish);
    }

    @Override
    public void deleteById(Long id) {
        //TODO: debug this
        //userClient.removeByReservedWishId(id);
        wishRepo.deleteById(id);
    }
}
