package api.wishList.web;


import api.wishList.entity.Wish;
import api.wishList.service.WishService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = "/wish")
public class WishControl {
    private final WishService wishService;

    public WishControl(@Qualifier("wishServiceImpl") WishService wishService) {
        this.wishService = wishService;
    }

    @GetMapping("/id={id}")
    public ResponseEntity<Wish> getById(@PathVariable Long id) {
        return new ResponseEntity<>(wishService.findById(id), HttpStatus.ACCEPTED);
    }

    @GetMapping("")
    public ResponseEntity<Collection<Wish>> getAll() {
        return new ResponseEntity<>(wishService.findAll(), HttpStatus.ACCEPTED);
    }

    @PostMapping("")
    public ResponseEntity<Wish> create(@RequestBody Wish wish) {
        return new ResponseEntity<>(wishService.save(wish), HttpStatus.CREATED);
    }

    @PutMapping("")
    public ResponseEntity<Wish> update(@RequestBody Wish wish) {
        return new ResponseEntity<>(wishService.save(wish), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable Long id) {
        System.out.println(id);
        wishService.deleteById(id);
        return new ResponseEntity<>(true, HttpStatus.ACCEPTED);
    }

}
