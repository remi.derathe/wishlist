package api.wishList.web;

import api.wishList.dtos.WishListDTO;
import api.wishList.entity.Wish;
import api.wishList.entity.WishList;
import api.wishList.model.RequestAddWish;
import api.wishList.service.WishListService;
import api.wishList.service.WishService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.util.Collection;

@RestController
@RequestMapping(value = "/wish_list")
public class WishListControl {
    private final WishListService wishListService;
    
    public WishListControl(WishListService wishListService) {
        this.wishListService = wishListService;
    }

    @GetMapping("/list/{listId}")
    public ResponseEntity<WishList> getByNameAndId(@PathVariable String listId) {
        return new ResponseEntity<>(wishListService.findById(Long.parseLong(listId.split("-")[1])), HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/id={id}", consumes = "application/json")
    public ResponseEntity<WishList> getById(@PathVariable Long id) {
        return new ResponseEntity<>(wishListService.findById(id), HttpStatus.ACCEPTED);
    }

    @GetMapping("")
    public ResponseEntity<Collection<WishList>> getAll() {
        return new ResponseEntity<>(wishListService.findAll(), HttpStatus.ACCEPTED);
    }

    @PostMapping("")
    public ResponseEntity<WishList> create(@RequestBody WishList wish) {
        return new ResponseEntity<>(wishListService.save(wish), HttpStatus.CREATED);
    }

    @PutMapping("")
    public ResponseEntity<WishList> update(@RequestBody WishList wishs) {
        return new ResponseEntity<>(wishListService.save(wishs), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("")
    public ResponseEntity<Boolean> delete(@RequestParam("id") Long wishListId) {
        wishListService.delete(wishListId);

        return new ResponseEntity<>(true, HttpStatus.ACCEPTED);
    }

    @PatchMapping("/addWish")
    public ResponseEntity<WishList> addWish(
        @RequestParam("idWishList") Long idWishList,
        @RequestParam("idWish") Long idWish
    ) {
        return new ResponseEntity<>(wishListService.addWish(idWishList, idWish), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/removeWish")
    public ResponseEntity<WishList> removeWish(
            @RequestParam("idWishList") Long idWishList,
            @RequestParam("idWish") Long idWish
    ) {
        return new ResponseEntity<>(wishListService.removeWish(idWishList, idWish), HttpStatus.ACCEPTED);
    }
}
