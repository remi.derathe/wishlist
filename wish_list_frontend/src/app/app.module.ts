import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {CreateWishlistFormComponent} from "./component/create-wishlist-form/create-wishlist-form.component";
import {CreateWishFormComponent} from "./component/create-wish-form/create-wish-form.component";
import {WishComponent} from "./component/wish/wish.component";
import {LoginPageComponent} from "./page/login-page/login-page.component";
import {RegisterPageComponent} from "./page/register-page/register-page.component";
import {HomePageComponent} from "./page/home-page/home-page.component";
import { WishListUserComponent } from './component/wish-list-user/wish-list-user.component';
import { WishListComponent } from './component/wish-list/wish-list.component';
import { ListPageComponent } from './page/list-page/list-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    RegisterPageComponent,
    HomePageComponent,
    CreateWishlistFormComponent,
    CreateWishFormComponent,
    WishComponent,
    WishListUserComponent,
    WishListComponent,
    ListPageComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
