import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWishFormComponent } from './create-wish-form.component';

describe('CreateWishFormComponent', () => {
  let component: CreateWishFormComponent;
  let fixture: ComponentFixture<CreateWishFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateWishFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateWishFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
