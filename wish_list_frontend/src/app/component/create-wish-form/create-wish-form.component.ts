import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LocalStorageWishForListService} from "../../service/local-storage-wish-for-list.service";
import {WishApiService} from "../../service/wish-api.service";
import {flush} from "@angular/core/testing";

@Component({
  selector: 'app-create-wish-form',
  templateUrl: './create-wish-form.component.html',
  styleUrls: ['./create-wish-form.component.css']
})
export class CreateWishFormComponent {
  createWish_form: FormGroup
  _localStorageList: LocalStorageWishForListService
  _wish_api_service: WishApiService
  _formIsVisible: Boolean

  constructor(private formBuilder: FormBuilder,
              private wish_api_service: WishApiService,
              private localStorageWishForListService:LocalStorageWishForListService) {
    this._formIsVisible = false
    this._localStorageList = localStorageWishForListService
    this._wish_api_service = wish_api_service
    this.createWish_form = this.formBuilder.group({
      name: ['', Validators.required],
      image: ['', Validators.required],
      depiction: ['', Validators.required],
      price: ['', Validators.required],
    }) }

  submitForm() {
    if (this.createWish_form.status === "VALID") {
      console.log(this.createWish_form.value)
      this._wish_api_service.postWish(this.createWish_form.value).subscribe((rep) => {
          let wishExisting = this.localStorageWishForListService.getWishForList()
          if (!wishExisting) wishExisting = []
          wishExisting.push(<Wish>rep)
          this._localStorageList.setWishForList(wishExisting)
          this.createWish_form.reset()
          this._formIsVisible = false
        })
    }
  }

  toggleForm() {
    this._formIsVisible = !this._formIsVisible
  }
}
