import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWishlistFormComponent } from './create-wishlist-form.component';

describe('CreateWishlistFormComponent', () => {
  let component: CreateWishlistFormComponent;
  let fixture: ComponentFixture<CreateWishlistFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateWishlistFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateWishlistFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
