import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {WishComponent} from "../wish/wish.component";
import {LocalStorageWishForListService} from "../../service/local-storage-wish-for-list.service";
import {WishApiService} from "../../service/wish-api.service";
import {TokenStorageService} from "../../service/token-storage.service";
import {UserService} from "../../service/user.service";

@Component({
  selector: 'app-create-wishlist-form',
  templateUrl: './create-wishlist-form.component.html',
  styleUrls: ['./create-wishlist-form.component.css']
})
export class CreateWishlistFormComponent {
  createWishList_form: FormGroup
  _localStorageList: LocalStorageWishForListService
  _wish_api: WishApiService
  _token_storage: TokenStorageService
  _user_api: UserService

  _formIsVisible: boolean
  constructor(private formBuilder: FormBuilder,
              private wish_api: WishApiService,
              private _localStorageWishForListService:LocalStorageWishForListService,
              private token_storage: TokenStorageService,
              private user_api: UserService
) {
    this._user_api = user_api
    this._formIsVisible = false
    this._wish_api = wish_api
    this._token_storage = token_storage
    this._localStorageList = _localStorageWishForListService
    this.createWishList_form = this.formBuilder.group({
      name: ['', Validators.required],
      depiction: ['', Validators.required],
    })
  }

  async submitForm() {
    if (this.createWishList_form.status === "VALID") {
      this.createWishList_form.value.list = this._localStorageList.getWishForList()
      this._user_api.getUserIdByToken(this._token_storage.token).subscribe((idUser) => {
        console.log(idUser)
        this.createWishList_form.value.ownerId = idUser
        console.log("valid", this.createWishList_form.value)
        this._wish_api.postWishList(this.createWishList_form.value)
          .subscribe((rep) => {
            this._localStorageWishForListService.removeStorage()
            window.location.reload()
          })
      })
    }
  }

  toggleForm() {
    this._formIsVisible = !this._formIsVisible
  }
}
