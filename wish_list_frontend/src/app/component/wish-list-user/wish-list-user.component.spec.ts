import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WishListUserComponent } from './wish-list-user.component';

describe('WishListUserComponent', () => {
  let component: WishListUserComponent;
  let fixture: ComponentFixture<WishListUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WishListUserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WishListUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
