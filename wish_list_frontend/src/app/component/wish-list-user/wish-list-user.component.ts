import {Component, OnInit} from '@angular/core';
import {WishApiService} from "../../service/wish-api.service";
import {UserService} from "../../service/user.service";
import {TokenStorageService} from "../../service/token-storage.service";

@Component({
  selector: 'app-wish-list-user',
  templateUrl: './wish-list-user.component.html',
  styleUrls: ['./wish-list-user.component.css']
})
export class WishListUserComponent implements OnInit {
  _wish_api: WishApiService
  _user_service: UserService
  _token_storage: TokenStorageService
  _wishList_user: WishList[] = []
  constructor(private wish_api: WishApiService,
              private user_service: UserService,
              private token_storage: TokenStorageService
  ) {
    this._token_storage = token_storage
    this._wish_api = wish_api
    this._user_service = user_service
  }

  ngOnInit(): void {
    const token = this._token_storage.token
    console.log(token)
    this._user_service.getUserIdByToken(token)
      .subscribe(id => {
        if (id) this._user_service.getUserById(Number(id))
          .subscribe((userStr) => {
            const user: User = JSON.parse(userStr)
            console.log()
            user.wishListId.forEach((idList) => {
              this._wish_api.getWishList(idList).subscribe((list) => {
                this._wishList_user.push(<WishList>list)
              })
            })
          })
      })
  }



}
