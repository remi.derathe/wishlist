import {AfterViewInit, Component, Input} from '@angular/core';
import {WishApiService} from "../../service/wish-api.service";
import {LocalStorageWishForListService} from "../../service/local-storage-wish-for-list.service";

@Component({
  selector: 'app-wish',
  templateUrl: './wish.component.html',
  styleUrls: ['./wish.component.css']
})
export class WishComponent {
  //TODO: find better solution that any
  @Input() item: Wish[] = []
  _wish_api_service: WishApiService
  _localStorageList: LocalStorageWishForListService

  constructor(private wish_api_service: WishApiService,
              private localStorageList: LocalStorageWishForListService) {
    this._wish_api_service = wish_api_service
    this._localStorageList = localStorageList
  }

  removeWish() {
    const id = this.item[0].id
    if (id) this._wish_api_service.deleteWish(id)
    const stored = this._localStorageList.getWishForList()
    this._localStorageList.setWishForList(stored.filter((elm) => this.item[0].id != elm.id))
  }
}
