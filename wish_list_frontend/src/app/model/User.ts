type User = {
  id: number
  mail: string
  pseudo: string
  presentation: string
  reservedId: number[]
  wishListId: number[]
}
