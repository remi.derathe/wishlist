type Wish = {
  id: number
  name: string
  image: string
  depiction: string
  price: number
}
