type WishList = {
  id: number
  name: string
  url: string
  list: Array<Wish>
  ownerId: number
}
