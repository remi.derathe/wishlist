import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor() {
    const token = localStorage.getItem("login-token")
    if (!token) {
      window.location.href = "/login"
    }
  }

  ngOnInit(): void {
  }

}
