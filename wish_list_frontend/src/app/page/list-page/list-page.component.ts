import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {WishApiService} from "../../service/wish-api.service";

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.css']
})
export class ListPageComponent implements OnInit {
  private _wish_api: WishApiService
  public wishListId: null|number = null
  public list: WishList[] = []
  constructor(
    private router : Router,
    private wish_api: WishApiService
  ) {
    this._wish_api = wish_api
  }

  ngOnInit() {
    const nameId = this.router.url.replace("/list/", '').split('-')
    this.wishListId = Number(nameId[nameId.length-1]);

    if (this.wishListId) {
      this._wish_api.getWishList(this.wishListId)
        .subscribe((list) => {
          this.list.push(<WishList>list)
        })
    }
  }

}
