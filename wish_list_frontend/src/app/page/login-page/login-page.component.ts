import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import axios from "axios";
import {TokenStorageService} from "../../service/token-storage.service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  loginForm: FormGroup
  _token_storage: TokenStorageService

  constructor(private formBuilder: FormBuilder,
              private token_storage: TokenStorageService) {
    this._token_storage = token_storage
    this.loginForm = this.formBuilder.group({
      userMail: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit() {
  }

  submitForm() {
    console.log(this.loginForm.value)
    if (this.loginForm.status === "VALID") {
      axios.post(
        "http://localhost:8080/login",
        this.loginForm.value,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      ).then((res) => {
        if (res.data) {
          this._token_storage.token = res.data
          window.location.href = "/"
        }
      }).catch((err) => {
        console.log("err") ;
      });
    }

  }
}
