import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validator,
  ValidatorFn,
  Validators
} from "@angular/forms";
import axios from "axios";
import {HttpHeaders, HttpRequest} from "@angular/common/http";

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {
  registerForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      password2: ['', Validators.required],
      pseudo: ['', Validators.required],
      presentation: [''],
    }, {validators: this.checkPasswords})
  }

  ngOnInit(): void {
  }

  async submitForm() {

    if (this.registerForm.status === "VALID") {
      const body = {
        "mail": this.registerForm.get("email")?.value,
        "password": this.registerForm.get("password")?.value,
        "pseudo": this.registerForm.get("pseudo")?.value,
        "presentation": this.registerForm.get("presentation")?.value,
      }
      axios.post(
        "http://localhost:8080/user",
        body,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      ).then((res) =>{
        console.log(res.data) ;
        if (res.data) {
          window.location.href = "/"
        }
      }).catch((err) => {
        console.log("err") ;
      });
    }

  }
  checkPasswords: ValidatorFn = (group: AbstractControl):  ValidationErrors | null => {
    let pass = group.get('password')?.value
    let confirmPass = group.get('password2')?.value
    return pass === confirmPass ? null : { notSame: true }
  }
}
