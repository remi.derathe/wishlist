import { TestBed } from '@angular/core/testing';

import { LocalStorageWishForListService } from './local-storage-wish-for-list.service';

describe('LocalStorageWishForListService', () => {
  let service: LocalStorageWishForListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalStorageWishForListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
