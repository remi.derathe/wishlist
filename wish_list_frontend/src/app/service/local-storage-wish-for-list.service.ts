import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageWishForListService {

  getWishForList(): Array<Wish> {
    const listString = localStorage.getItem("wishForList")
    if (listString) return JSON.parse(listString)
    return []
  }

  setWishForList(data: Array<Wish>) {
    localStorage.setItem("wishForList", JSON.stringify(data))
  }

  removeStorage() {
    localStorage.removeItem("wishForList")
  }
}
