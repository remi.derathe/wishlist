import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }

  get token(){
    const token = localStorage.getItem("login-token")
    if (token) return token
    return ""
  }

  set token(token: string) {
    localStorage.setItem("login-token", token)
  }
  public clear() {
    localStorage.removeItem("login-token")
    window.location.reload()
  }

}
