import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError} from "rxjs";
import {TokenStorageService} from "./token-storage.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private BASE_URL = "http://localhost:8080"
  private _token_storage: TokenStorageService
  constructor(
    private httpClient: HttpClient,
    private token_storage: TokenStorageService
  ) {
    this._token_storage = token_storage
  }

  getUserIdByToken(token: string) {
    return this.httpClient.get(
      `${(this.BASE_URL)}/user/getUserIdByToken`,
      {
        responseType: "text",
        params: {
          token: token
        },
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
    //   .pipe(
    //   catchError(async () => this._token_storage.clear()  )
    // )
  }
  getUserById(id: number) {
    return this.httpClient.get(
      `${(this.BASE_URL)}/user/id=${id}`,
      {
        responseType: "text",
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
  }
}
