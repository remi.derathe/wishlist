import { Injectable } from '@angular/core';
import axios from "axios";
import {HttpClient} from "@angular/common/http";
import {Observable, tap} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class WishApiService {
  private BASE_URL = "http://localhost:8080"
  constructor(private httpClient: HttpClient) {  }


  postWish(wish:Wish): Observable<Object> {
    return this.httpClient.post(
      `${this.BASE_URL}/wish`,
      wish
    )
  }
  postWishList(wishList:WishList) {
    return this.httpClient.post(
      `${this.BASE_URL}/wish_list`,
      wishList,
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
  }

  deleteWish(id:number) {
    axios.delete(`http://localhost:8080/wish/${id}`)
  }

  getWishList(id:number) {
    return this.httpClient.get(
      `${this.BASE_URL}/wish_list/id=${id}`,
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
  }
}
